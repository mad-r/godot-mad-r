class_name NodeHelper

static func removeChildren(node:Node)->void:
	while node.get_child_count() > 0:
		node.remove_child(node.get_child(0))
