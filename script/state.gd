class_name State

enum StateValues{
	NONE,
	L,
	H,
	Z,
	X,
	SHORTED
}


var current := StateValues.NONE
var next := StateValues.NONE


func setToNext()->void:
	if next != StateValues.NONE:
		current = next
		next = StateValues.NONE


func setNext(state:StateValues)->void:
	if next == StateValues.NONE or next == StateValues.Z or next == state:
		next = state
	match(state):
		StateValues.L:
			if next == StateValues.H:
				next = StateValues.SHORTED
		StateValues.H:
			if next == StateValues.L:
				next = StateValues.SHORTED
		StateValues.Z:
			pass
		StateValues.X:
			next = state
		StateValues.SHORTED:
			pass
		_:
			push_error("assigning to shorted")


static func stateValueToColor(state:StateValues)->Color:
	match(state):
		StateValues.NONE:
			return Color.WHITE
		StateValues.L:
			return Color.BLUE
		StateValues.H:
			return Color.RED
		StateValues.Z:
			return Color.BLACK
		StateValues.X:
			return Color.BLUE_VIOLET
		StateValues.SHORTED:
			return Color.YELLOW
		_:
			push_error()
			return Color.PINK
