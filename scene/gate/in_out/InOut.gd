@tool

class_name InOut
extends Control

signal addWire

@export var gate:Gate = null

@export var color := Color('2e2e2e') :
	set(value):
		color = value
		queue_redraw()

var _wires := []


func _draw()->void:
	var m = min(size.x,size.y)
	set_size(Vector2(m,m))
	var radius = m/2
	color = State.stateValueToColor(getState())
	draw_circle(Vector2(radius,radius),radius,color)


func _gui_input(event:InputEvent)->void:
	if event.is_action_pressed("inout_add_wire"):
		addWire.emit()


func connectWire(wire:Wire)->void:
	if not _wires.has(wire):
		_wires.append(wire)
		wire.remove.connect(func():
			disconnectWire(wire)
		)


func disconnectWire(wire:Wire)->void:
	var index := _wires.find(wire)
	if index != -1:
		_wires.remove_at(index)


func getWires()->Array:
	return _wires


func setNext(state:State.StateValues)->void:
	for wire in _wires:
		(wire as Wire).setNextState(state)
		wire.queue_redraw()
	queue_redraw()


func getState()->State.StateValues:
	var state := State.StateValues.NONE
	for wire in _wires:
		var ws = (wire as Wire).getState()
		if state == State.StateValues.NONE:
			state = ws
		elif state != ws:
			state = State.StateValues.SHORTED
			setNext(state)
	return state
