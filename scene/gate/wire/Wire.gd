@tool

class_name Wire
extends Control

signal remove

@export var color := Color.GRAY
@export var left : InOut = null :
	set(value):
		if left != null:
			left.disconnectWire(self)
		left = value
		left.connectWire(self)
@export var right : InOut = null :
	set(value):
		if right != null:
			right.disconnectWire(self)
		right = value
		right.connectWire(self)
@export var width := 5
@export var antialiased := true

@export var points :Array = []

@export var isSelected := false :
	set(value):
		isSelected = value
		queue_redraw()

var _state := State.new()

@onready var pointHolder := $PointHolder
@onready var rightClickMenu := $RightClickMenu

func _process(_delta):
	if isSelected:
		queue_redraw()


func _draw():
	color = State.stateValueToColor(_state.current)
	
	var nodes = []
	if left != null:
		nodes.append(left)
	nodes.append_array(nodePathsToNodes(points))
	if right != null:
		nodes.append(right)
	var pointsToDraw := getPointsToDraw(nodes)
	if isSelected:
		if pointsToDraw.size() > 1:
			pointsToDraw.append(pointsToDraw[-1])
		pointsToDraw.append(get_local_mouse_position())
	for point in pointsToDraw:
		draw_circle(point,width/2.0,color)
	if pointsToDraw.size() >= 2:
		draw_multiline(pointsToDraw,color,width)


func getPointsToDraw(nodes:Array)->PackedVector2Array:
	if nodes.size() <= 2:
		return getLocationsFromNode(nodes)
	var pointsToDraw := [nodes[0]]
	for i in range(1,nodes.size()-1):
		pointsToDraw.append(nodes[i])
		pointsToDraw.append(nodes[i])
	pointsToDraw.append(nodes[-1])
	return getLocationsFromNode(pointsToDraw)


func getLocationsFromNode(nodes:Array)->PackedVector2Array:
	var locations := []
	for node in nodes:
		locations.append(getLocalizedPos(node))
	return locations


func getLocalizedPos(point:Control)->Vector2:
	return point.global_position - global_position


func _unhandled_input(event):
	if isSelected:
		if event.is_action_pressed("wire_add_point"):
			addNewPoint(get_local_mouse_position())
			queue_redraw()
			get_tree().root.set_input_as_handled()
		if event.is_action_pressed("wire_remove_selected"):
			if left == null or right == null:
				removeSelf()
			else:
				isSelected = false
	else:
		if event.is_action_pressed("wire_open_menu") and isPositionIn(event.global_position-position):
			showMenu()


func isPositionIn(pos:Vector2)->bool:
	pos += PositionHelper.cornerOfScreen
	var nodes = []
	if left != null:
		nodes.append(left)
	nodes.append_array(nodePathsToNodes(points))
	if right != null:
		nodes.append(right)
	var positions := getLocationsFromNode(nodes)
	print(positions)
	for i in positions.size()-1:
		var p0 = positions[i]
		var p1 = positions[i+1]
		if isBetween(pos,p0,p1):
			return true
	return false


func isBetween(pos:Vector2,p0:Vector2,p1:Vector2)->bool:
	var squared_width := width*width
	var closest_point := Geometry2D.get_closest_point_to_segment(pos, p0, p1)
	print(closest_point.distance_squared_to(pos))
	print(pos)
	return closest_point.distance_squared_to(pos) <= squared_width


func showMenu()->void:
	rightClickMenu.popup()
	rightClickMenu.position = PositionHelper.mousePosOnScreen


func removeSelf()->void:
	remove.emit()


func addNewPoint(pos:Vector2)->void:
	var point := newPointAt(pos)
	pointHolder.add_child(point)
	point.owner = pointHolder
	points.append(point.get_path())


func newPointAt(pos:Vector2)->Control:
	var point := Control.new()
	point.position = pos
	return point


func nodePathsToNodes(paths:Array)->Array:
	return paths.map(func (path:NodePath):
		return get_node(path)
	)


func setNextState(state:State.StateValues)->void:
	_state.setNext(state)


func setStateToNext()->void:
	_state.setToNext()


func getState()->State.StateValues:
	return _state.current


func _on_right_click_menu_id_pressed(id:int)->void:
	match(id):
		0:
			removeSelf()
		_:
			push_error()
