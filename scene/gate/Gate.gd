@tool

class_name Gate
extends Control

signal remove

const InOutScene = preload("res://scene/gate/in_out/InOut.tscn")

signal addWire(inout:InOut)

@export_category("Inout")
@export var inputCount := 0 :
	set(value):
		inputCount = value
		queue_redraw()

@export var outputCount := 0 :
	set(value):
		outputCount = value
		queue_redraw()

@export_category("Visual")
@export var color := Color('cccccc') :
	set(value):
		color = value
		queue_redraw()

@export var borderColor := Color('000000') :
	set(value):
		borderColor = value
		queue_redraw()

@export var borderSize := 2 :
	set(value):
		borderSize = value
		queue_redraw()

@export_category("Text")
@export var text := "" :
	set(value):
		text = value
		queue_redraw()

@export var textColor := Color('000000') :
	set(value):
		textColor = value
		queue_redraw()

@export var textSize := 20 :
	set(value):
		textSize = value
		queue_redraw()

@export var hideText := false :
	set(value):
		hideText = value
		queue_redraw()

@onready var inputContainer := $BodyMargin/BodyBox/InputContainer
@onready var outputContainer := $BodyMargin/BodyBox/OutputContainer
@onready var label := $BodyMargin/BodyBox/Label
@onready var bg := $BGContainer/BG
@onready var border := $Border
@onready var bgContainer := $BGContainer
@onready var rightClickMenu := $RightClickMenu

var _isDragging := false
var _draggedPos:Vector2
var _startingStraightPos:Vector2
var _isStraightDrag := false

var _wires := []

func _draw():
	print("draw of gate")
	_setInputChildren()
	_setOutputChildren()
	label.text = "" if hideText else text
	label.set("theme_override_colors/font_color",textColor)
	label.set("theme_override_font_sizes/font_size",textSize)
	bg.color = color
	_setBorder()
	updateWires()


func _setBorder():
	border.color = borderColor
	bgContainer.set("theme_override_constants/margin_top",borderSize)
	bgContainer.set("theme_override_constants/margin_right",borderSize)
	bgContainer.set("theme_override_constants/margin_bottom",borderSize)
	bgContainer.set("theme_override_constants/margin_left",borderSize)


func _setInputChildren():
	_setInOutChildren(inputContainer,inputCount)


func _setOutputChildren():
	_setInOutChildren(outputContainer,outputCount)


func _setInOutChildren(container:VBoxContainer,count:int):
	if container.get_child_count() < count:
		while count > container.get_child_count():
			addNewInoutTo(container)
	else:
		while count < container.get_child_count():
			container.remove_child(container.get_child(0))


func addNewInoutTo(container:VBoxContainer)->void:
	var inout = InOutScene.instantiate()
	container.add_child(inout)
	inout.owner = container
	inout.gate = self
	inout.addWire.connect(func ()->void:
		_onInOut_addWire(inout)
	)


func _on_margin_container_resized():
	set_custom_minimum_size($BodyMargin.size)
	update_minimum_size()
	set_size($BodyMargin.size)


func _gui_input(event:InputEvent)->void:
	if event.is_action_pressed("gate_drag"):
		if not _isDragging:
			_draggedPos = event.position
			_isDragging = true
			return
	elif event.is_action_released("gate_drag"):
		_isDragging = false
	elif event.is_action_pressed("gate_open_menu"):
		openMenu()
	
	if _isDragging:
		_handleDragging()


func _onInOut_addWire(inout:InOut)->void:
	addWire.emit(inout)


func _handleDragging()->void:
	var toPos = get_global_mouse_position()-_draggedPos
	if Input.is_action_pressed("gate_drag_straight"):
		if not _isStraightDrag:
			_isStraightDrag = true
			_startingStraightPos = global_position
		var diff = toPos-_startingStraightPos
		global_position = _startingStraightPos
		if abs(diff.y) > abs(diff.x):
			global_position.y = toPos.y
		else:
			global_position.x = toPos.x
	else:
		_isStraightDrag = false
		global_position = toPos
	updateWires()


func addToWires(wire:Wire)->void:
	if not _wires.has(wire):
		_wires.append(wire)
		wire.remove.connect(func():
			removeFromWires(wire)
		)


func removeFromWires(wire:Wire)->void:
	var index := _wires.find(wire)
	if index != -1:
		_wires.remove_at(index)


func updateWires():
	for wire in _wires:
		wire.queue_redraw()


func removeAllWires()->void:
	for wire in _wires:
		wire.removeSelf()


func getInputs()->Array:
	return inputContainer.get_children()


func getOutputs()->Array:
	return outputContainer.get_children()


func openMenu()->void:
	rightClickMenu.popup()
	rightClickMenu.position = PositionHelper.mousePosOnScreen


func _on_right_click_menu_id_pressed(id:int)->void:
	match(id):
		0:
			remove.emit()
			removeAllWires()
		_:
			push_error()


#func serialize()->Dictionary:
	#return {
		#"inputCount":inputCount,
		#"outputCount":outputCount,
		#"color":color,
		#"borderColor":borderColor,
		#"borderSize":borderSize,
		#"text":text,
		#"textColor":textColor,
		#"textSize":textSize,
		#"hideText":hideText,
		#"global_position":global_position,
		##_wires
		##inouts
		##additionalContainer
	#}
