extends Control

signal newBoard
signal loadBoard
signal settings
signal exit



func _on_new_board_pressed():
	newBoard.emit()


func _on_load_board_pressed():
	loadBoard.emit()


func _on_settings_pressed():
	settings.emit()


func _on_exit_pressed():
	exit.emit()
