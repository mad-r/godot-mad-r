extends Control

signal save
signal load
signal mainMenu

signal assetClick(id:int)


@export var assetNames := [] :
	set(value):
		assetNames = value
		queue_redraw()

@onready var menu := $Menu
@onready var assets := $Assets

func _draw():
	_resetAssets()


func _ready():
	menu.get_popup().id_pressed.connect(func (id:int)->void:
		_onMenuItemClick(id)
	)
	assets.get_popup().id_pressed.connect(func (id:int)->void:
		assetClick.emit(id)
	)
	_resetAssets()


func _resetAssets():
	var popup:PopupMenu = assets.get_popup()
	popup.clear()
	for i in assetNames.size():
		popup.add_item(assetNames[i],i)


func _onMenuItemClick(id:int)->void:
	match(id):
		0:
			save.emit()
		1:
			load.emit()
		2:
			mainMenu.emit()
		_:
			push_error()
