extends Camera2D

@export var speed := 800
@export var zoomSpeed := 20

@onready var fps := $FPS
@onready var boardBar = $BoardBar

var draggedPos = Vector2.ZERO
var isDragging = false



func _process(delta:float)->void:
	moveCamera(delta)
	zoomCamera(delta)
	doFPSStuff()
	doBoardBarStuff()
	PositionHelper.mousePosOnScreen = get_global_mouse_position()-fps.global_position
	PositionHelper.cornerOfScreen = fps.global_position

func doBoardBarStuff()->void:
	boardBar.scale = Vector2(1/zoom.x,1/zoom.y)
	var cameraRect = getCameraRect()
	boardBar.position = Vector2(-cameraRect.x/2,(cameraRect.y/2)-boardBar.get_rect().size.y)

func doFPSStuff()->void:
	fps.text = "FPS: %d"%Engine.get_frames_per_second()
	fps.scale = Vector2(1/zoom.x,1/zoom.y)
	fps.position = -getCameraRect()/2

func getCameraRect()->Vector2:
	return get_canvas_transform().affine_inverse().basis_xform(get_viewport_rect().size)

func moveCamera(delta:float)->void:
	doDragStuff()
	global_position += getMoveDir()*speed*delta

func doDragStuff()->void:
	if Input.is_action_pressed("camera_drag"):
		var mousePos = get_global_mouse_position()
		if isDragging:
			global_position += draggedPos-mousePos
		else:
			draggedPos = mousePos
			isDragging = true
	elif Input.is_action_just_released("camera_drag"):
		isDragging = false

func getMoveDir()->Vector2:
	var dir := Vector2(
		Input.get_action_strength("camera_move_right")-Input.get_action_strength("camera_move_left"),
		Input.get_action_strength("camera_move_down")-Input.get_action_strength("camera_move_up")
	)
	
	return dir

func zoomCamera(delta:float)->void:
	zoom += zoom*getZoomChange()*zoomSpeed*delta
	if zoom < Vector2(0.1,0.1):
		zoom = Vector2(0.1,0.1)
	elif zoom > Vector2(9,9):
		zoom = Vector2(9,9)

func getZoomChange()->int:
	var z = 0
	if Input.is_action_just_pressed("camera_zoom_in"):
		z += 1
	if Input.is_action_just_pressed("camera_zoom_out"):
		z -= 1
	return z
