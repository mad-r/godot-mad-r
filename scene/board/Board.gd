extends Control

signal mainMenu

const GateScene := preload("res://scene/gate/Gate.tscn")
const WireScene := preload("res://scene/gate/wire/Wire.tscn")
const InputScene := preload("res://scene/gates/input/Input.tscn")
const NandScene := preload("res://scene/gates/nand/Nand.tscn")
const TBuffScene := preload("res://scene/gates/tbuff/TBuff.tscn")
const OutputScene := preload("res://scene/gates/output/Output.tscn")

@onready var camera := $BoardCamera
@onready var boardBar := $BoardCamera/BoardBar
@onready var gateHolder := $GateHolder
@onready var wireHolder := $WireHolder

var gates := [
	InputScene,
	NandScene,
	OutputScene,
	TBuffScene,
]

func _ready():
	boardBar.save.connect(_onBoardBarSave)
	boardBar.load.connect(_onBoardBarLoad)
	boardBar.mainMenu.connect(_onBoardBarMainMenu)
	boardBar.assetClick.connect(_onBoardBarAssetClick)
	boardBar.assetNames = gates.map(func (gate):
		return gate.instantiate().text
	)

var isHanded := true

func _process(delta):
	if isHanded or Input.is_action_just_pressed("board_calc"):
		calc()
	if Input.is_action_just_pressed("board_handed"):
		isHanded = not isHanded

func calc()->void:
	for gate in gateHolder.get_children():
		gate.calc()
	for wire in wireHolder.get_children():
		(wire as Wire).setStateToNext()


func getCenterOfScreen()->Vector2:
	return camera.position


func _onBoardBarSave()->void:
	print("save")


func _onBoardBarLoad()->void:
	print("load")


func _onBoardBarMainMenu()->void:
	mainMenu.emit()


func _onBoardBarAssetClick(id:int)->void:
	addNewGate(id)


func addNewGate(id:int)->void:
	var gate:Gate = gates[id].instantiate()
	gateHolder.add_child(gate)
	gate.owner = gateHolder
	gate.position = getCenterOfScreen()
	gate.addWire.connect(_onGateAddWire)
	gate.remove.connect(func():
		_onGateRemove(gate)
	)


func _onGateAddWire(inout:InOut)->void:
	var selectedWire := getSelectedWire()
	if selectedWire == null:
		var wire := newWire()
		wire.left = inout
		inout.gate.addToWires(wire)
		wire.isSelected = true
	else:
		selectedWire.isSelected = false
		selectedWire.right = inout
		inout.gate.addToWires(selectedWire)


func _onGateRemove(gate:Gate)->void:
	gateHolder.remove_child(gate)
	gate.queue_free()


func newWire()->Wire:
	var wire := WireScene.instantiate()
	wireHolder.add_child(wire)
	wire.owner = wireHolder
	wire.remove.connect(func ():
		_onWireRemove(wire)
	)
	return wire


func _onWireRemove(wire:Wire)->void:
	print("wire remove")
	wireHolder.remove_child(wire)
	wire.queue_free()


func getSelectedWire()->Wire:
	for wire in wireHolder.get_children():
		if wire.isSelected:
			return wire
	return null

