@tool

class_name Circle
extends Control

@export var radius := 10.0 :
	set(value):
		radius = value
		queue_redraw()

@export var color := Color('eee') :
	set(value):
		color = value
		queue_redraw()

func _draw():
	draw_circle(Vector2(radius,radius),radius,color)
	var newSize = Vector2(radius*2,radius*2)
	set_size(newSize)
	set_custom_minimum_size(newSize)
