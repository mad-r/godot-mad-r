@tool

extends Gate


func calc():
	var input0:InOut = getInputs()[0]
	var input1:InOut = getInputs()[1]
	input0.queue_redraw()
	input1.queue_redraw()
	var s0 := input0.getState()
	var s1 := input1.getState()
	
	var state := State.StateValues.NONE
	
	match(s1):
		State.StateValues.H:
			state = s0
		State.StateValues.NONE, State.StateValues.Z, State.StateValues.L:
			state = State.StateValues.Z
		_:
			state = State.StateValues.X
	
	var output:InOut = getOutputs()[0]
	output.setNext(state)
