@tool

extends Gate

@onready var colorRect := $BodyMargin/BodyBox/AdditionalContainer/ColorRect

func calc():
	var input:InOut = getInputs()[0]
	input.queue_redraw()
	var state = input.getState()
	# input.setNext(state)
	colorRect.color = State.stateValueToColor(state)
