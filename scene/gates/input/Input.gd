@tool

extends Gate


var state := State.StateValues.L

@onready var button := $BodyMargin/BodyBox/AdditionalContainer/CheckButton

func _ready():
	button.set_pressed_no_signal(false)
	button.toggled.connect(func(toggled_on:bool):
		state = State.StateValues.H if toggled_on else State.StateValues.L
	)

func calc():
	var output:InOut = getOutputs()[0]
	output.setNext(state)

