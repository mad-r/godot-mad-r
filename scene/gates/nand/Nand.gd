@tool

extends Gate


func calc():
	var input0:InOut = getInputs()[0]
	var input1:InOut = getInputs()[1]
	input0.queue_redraw()
	input1.queue_redraw()
	var s0 := input0.getState()
	var s1 := input1.getState()
	
	var state := State.StateValues.NONE
	
	if s0 == State.StateValues.L or s1 == State.StateValues.L:
		state = State.StateValues.H
	elif s0 == State.StateValues.H and s1 == State.StateValues.H:
		state = State.StateValues.L
	else:
		state = State.StateValues.X
	
	var output:InOut = getOutputs()[0]
	output.setNext(state)
