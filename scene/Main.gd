extends Control


const BoardScene := preload("res://scene/board/Board.tscn")


@onready var mainMenu := $MainMenu
@onready var boardHolder := $BoardHolder


func _on_main_menu_new_board()->void:
	mainMenu.hide()
	_getNewBoard()


func _on_main_menu_load_board()->void:
	pass


func _on_main_menu_settings()->void:
	pass


func _on_main_menu_exit()->void:
	get_tree().quit()


func _getNewBoard()->void:
	NodeHelper.removeChildren(boardHolder)
	var board := BoardScene.instantiate()
	boardHolder.add_child(board)
	board.mainMenu.connect(_onBoardMainMenu)

func _onBoardMainMenu():
	NodeHelper.removeChildren(boardHolder)
	mainMenu.show()
